package cn.xiaohaoo.admin;

import cn.xiaohaoo.admin.service.MedicineService;
import lombok.extern.slf4j.Slf4j;
import org.bson.Document;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.CreateIndexResponse;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.QueryStringQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

@SpringBootTest
@Slf4j
class AdminApplicationTests {

    @Autowired
    private RestHighLevelClient restHighLevelClient;


    @Test
    public void addDocument() throws IOException {
        List<Document> medicineDetail = mongoTemplate.findAll(Document.class, "medicine_detail");
        BulkRequest bulkRequest = new BulkRequest();
        for (Document document : medicineDetail) {
            document.remove("_id");
            document.remove("img");
            bulkRequest.add(new IndexRequest("medicine_detail").source(document.toJson(), XContentType.JSON));
        }
        BulkResponse bulkResponses = restHighLevelClient.bulk(bulkRequest, RequestOptions.DEFAULT);
        System.out.println("bulkResponses.hasFailures() = " + bulkResponses.hasFailures());

    }


    /**
     * 创建Elasticsearch索引
     *
     * @throws IOException
     */
    @Test
    public void createIndex() throws IOException {
        CreateIndexRequest request = new CreateIndexRequest("medicine_detail");

        XContentBuilder settingsBuilder = XContentFactory.jsonBuilder()
            .startObject()
            .startObject("analysis")
            .field("analyzer", "ik_max_word")
            .field("search_analyzer", "ik_max_word")
            .endObject()
            .endObject();
        request.settings(settingsBuilder);
        CreateIndexResponse createIndexResponse = restHighLevelClient.indices().create(request, RequestOptions.DEFAULT);
        log.info(String.valueOf(createIndexResponse));
    }

    @Test
    public void deleteIndex() throws IOException {
        DeleteIndexRequest medicine = new DeleteIndexRequest("medicine_detail");
        restHighLevelClient.indices().delete(medicine, RequestOptions.DEFAULT);
    }


    @Test
    public void searchDocuments() throws IOException {
        QueryStringQueryBuilder queryBuilder = QueryBuilders.queryStringQuery("人参");

        SearchSourceBuilder searchBuilder = new SearchSourceBuilder();
        searchBuilder.query(queryBuilder);
        SearchRequest request = new SearchRequest("medicine_detail");

        HighlightBuilder highlightBuilder = new HighlightBuilder().field("*").requireFieldMatch(false);
        highlightBuilder.preTags("<span style=\"color:red\">");
        highlightBuilder.postTags("</span>");
        searchBuilder.highlighter(highlightBuilder);
        request.source(searchBuilder);
        SearchResponse response = restHighLevelClient.search(request, RequestOptions.DEFAULT);
        SearchHit[] searchHits = response.getHits().getHits();
        for (SearchHit hit : searchHits) {
            for (HighlightField field : hit.getHighlightFields().values()) {
                StringBuilder stringBuilder = new StringBuilder();
                for (Text fragment : field.fragments()) {
                    stringBuilder.append(fragment.toString());
                }
                log.info(stringBuilder.toString());
            }
        }
    }

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private MedicineService medicineService;

    @Test
    public void test02() {
        List<Document> medicineDetail = mongoTemplate.findAll(Document.class, "medicine_detail");

        medicineDetail.forEach(System.out::println);
    }

    public static void main(String[] args) {
        ReentrantLock reentrantLock = new ReentrantLock();
        reentrantLock.lock();
        reentrantLock.unlock();
    }
}
