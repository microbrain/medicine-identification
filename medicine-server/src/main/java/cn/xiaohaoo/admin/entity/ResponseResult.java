package cn.xiaohaoo.admin.entity;

import lombok.Getter;

@Getter
public class ResponseResult<T> {
    private final Integer code;
    private final T data;
    private final String message;

    private ResponseResult(Integer code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }


    public static <T> ResponseResult<T> success(T data) {
        return new ResponseResult<>(ResultCode.SUCCESS, ResultCode.NO_CONTENT_MESSAGE, data);
    }

    public static ResponseResult<Void> success(String message) {
        return new ResponseResult<>(ResultCode.SUCCESS, ResultCode.NO_CONTENT_MESSAGE, null);
    }

    public static ResponseResult<Void> success() {
        return new ResponseResult<>(ResultCode.SUCCESS, ResultCode.NO_CONTENT_MESSAGE, null);
    }

    public static <T> ResponseResult<T> success(String message, T data) {
        return new ResponseResult<>(ResultCode.SUCCESS, message, data);
    }

    public static ResponseResult<Void> error(String message) {
        return new ResponseResult<>(ResultCode.ERROR, message, null);
    }

    public static <T> ResponseResult<T> error(T data) {
        return new ResponseResult<>(ResultCode.ERROR, ResultCode.NO_CONTENT_MESSAGE, data);
    }

    public static <T> ResponseResult<T> error(String message, T data) {
        return new ResponseResult<>(ResultCode.ERROR, message, data);
    }

    public static <Void> ResponseResult<Void> error() {
        return new ResponseResult<>(ResultCode.ERROR, ResultCode.NO_CONTENT_MESSAGE, null);
    }

    private static final class ResultCode {
        private static final Integer SUCCESS = 200;
        private static final Integer ERROR = 500;
        private static final String NO_CONTENT_MESSAGE = "";
        private static final String ERROR_MESSAGE = "服务器错误，请联系管理员";
        private static final String SUCCESS_MESSAGE = "成功";
    }

}
