package cn.xiaohaoo.admin.mapper;

import cn.xiaohaoo.admin.entity.History;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface HistoryMapper extends BaseMapper<History> {

}
