import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:medicine_app/request_util.dart';

class MedicineDetailPage extends StatefulWidget {
  final String _name;

  MedicineDetailPage({Key? key, required String name})
      : _name = name,
        super(key: key);

  @override
  _MedicineDetailPageState createState() => _MedicineDetailPageState();
}

class _MedicineDetailPageState extends State<MedicineDetailPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          color: Colors.black,
          icon: Icon(Icons.arrow_back_ios),
          iconSize: 18,
          onPressed: () => Navigator.of(context).pop(),
        ),
        backgroundColor: Colors.white,
        centerTitle: true,
        elevation: 0.4,
        toolbarHeight: 55,
        title: Text(
          widget._name,
          style: TextStyle(color: Colors.black, fontSize: 16),
        ),
      ),
      body: FutureBuilder(
        future: Future.sync(() async {
          final response = await Request.getDio().get('/medicine/details', queryParameters: {'name': widget._name});
          return response.data['data'] as Map<String, dynamic>;
        }),
        builder: (BuildContext context, AsyncSnapshot<Map<String, dynamic>> snapshot) {
          final entries =
              snapshot.data?.entries.where((element) => element.key != '_id' && element.key != 'img').toList();
          if (snapshot.connectionState == ConnectionState.done) {
            final maxLength = entries?.length ?? 0;
            return ListView.separated(
                padding: EdgeInsets.symmetric(vertical: 15, horizontal: 25),
                itemBuilder: (BuildContext context, int index) {
                  if (index == 0 && snapshot.data?['img'] != null) {
                    return FractionallySizedBox(
                      widthFactor: 0.9,
                      child: ClipRRect(child: Image.network(snapshot.data?['img'])),
                    );
                  }
                  if (index < maxLength) {
                    return Container(
                      margin: EdgeInsets.symmetric(vertical: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Icon(
                                Icons.workspaces_filled,
                                color: Theme.of(context).primaryColor,
                                size: 12,
                              ),
                              Container(
                                padding: EdgeInsets.only(left: 5),
                                child: Text(
                                  entries?[index].key ?? '',
                                  style: TextStyle(fontWeight: FontWeight.w600, fontSize: 18),
                                ),
                              ),
                            ],
                          ),
                          RichText(
                              textAlign: TextAlign.left,
                              text: TextSpan(
                                  text: entries?[index].value.toString() ?? '',
                                  style: TextStyle(color: Colors.black87, fontSize: 14, height: 3))),
                        ],
                      ),
                    );
                  } else {
                    return Center(
                      child: Text(
                        '- 没有更多了 -',
                        style: TextStyle(color: Colors.grey, height: 2.5),
                      ),
                    );
                  }
                },
                separatorBuilder: (_, int index) {
                  if (index == 0 || index == maxLength - 1) {
                    return SizedBox.shrink();
                  } else {
                    return Divider(
                        height: 0.8, color: Theme.of(context).primaryColor.withAlpha(120));
                  }
                },
                itemCount: maxLength + 1);
          } else {
            return Center(
                child: SpinKitFadingCircle(
              color: Theme.of(context).primaryColor,
              size: 35,
            ));
          }
        },
      ),
    );
  }
}
