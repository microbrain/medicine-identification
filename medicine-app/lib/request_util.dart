import 'package:dio/dio.dart';

import 'main.dart';

class Request {
  static late final Dio _dio;

  static Dio getDio() {
    return _dio;
  }

  static void initialize() {
    final options;
    if (isDebug()) {
      options = BaseOptions(
        baseUrl: 'http://10.10.11.253:1433',
        // baseUrl: 'http://210.16.189.8:21433',
        connectTimeout: 5000,
        receiveTimeout: 3000,
      );
    } else {
      options = BaseOptions(
        baseUrl: 'http://210.16.189.8:21433',
        connectTimeout: 5000,
        receiveTimeout: 3000,
      );
    }
    _dio = Dio(options);
  }
}
