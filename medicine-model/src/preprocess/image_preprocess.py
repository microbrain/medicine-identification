import os
from keras_preprocessing import image
import tensorflow as tf

'''
对数据集的处理，统一处理图片大小为299*299
'''

dataset_dir = '../../../datasets/dataset'
img_size = (299, 299)

if __name__ == '__main__':
    for dir in os.listdir(dataset_dir):
        for file in os.listdir(os.path.join(dataset_dir, dir)):
            img_path = os.path.join(dataset_dir, dir, file)
            with open(img_path, 'rb') as f:
                img_contant = tf.image.decode_image(f.read())
                img_resize_float = tf.image.resize(
                    images=img_contant,
                    size=img_size,
                    method='nearest')
                img_resize = tf.image.convert_image_dtype(img_resize_float, dtype=tf.uint8)
            print('图片大小处理：'+file)
            image.save_img(img_path,img_resize)