## 主要功能展示

### 首页及中药信息页
<img src="../../assets/images/fun-0-1.png" width="220"/>
<img src="../../assets/images/fun-0-2.png" width="220"/>

### 拍照识别
点击导航栏拍照按钮，拍摄中药的图片上传后进行识别。

<img src="../../assets/images/fun-1-1.png" width="220"/>
<img src="../../assets/images/fun-1-2.png" width="220"/>
<img src="../../assets/images/fun-1-3.png" width="220"/>
<img src="../../assets/images/fun-1-4.png" width="220"/>


### 分类检索
点击右上方筛选按钮，对中药进行分类检索。

<img src="../../assets/images/fun-2-1.png" width="220"/>
<img src="../../assets/images/fun-2-2.png" width="220"/>

### 全文检索
输入关键词对中药库进行检索，对关键词进行粉刺搜索并高亮显示。

<img src="../../assets/images/fun-3-1.png" width="220"/>
<img src="../../assets/images/fun-3-2.png" width="220"/>


### 问题社区
设立中医药问题讨论区，可以提出问题或者回答。

<img src="../../assets/images/fun-4-1.png" width="220"/>
<img src="../../assets/images/fun-4-2.png" width="220"/>
